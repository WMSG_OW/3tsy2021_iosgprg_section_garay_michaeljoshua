using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoManager : MonoBehaviour
{
    public GameObject[] Ammo;

    public float xBounds, yBound;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnAweapon());
    }

    IEnumerator SpawnAweapon()
    {
        yield return new WaitForSeconds(3);
        Vector2 spawnPoint = new Vector2(Random.Range(-xBounds, xBounds), Random.Range(-yBound, yBound));
        if (GameObject.FindGameObjectsWithTag("Weapon").Length < 3)
            Instantiate(Ammo[Random.Range(0, Ammo.Length)], spawnPoint, Quaternion.identity);
        StartCoroutine(SpawnAweapon());
    }
}
