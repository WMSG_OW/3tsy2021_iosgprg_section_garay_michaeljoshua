using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class GameManager : MonoBehaviour
{
    public Player player;


   public void Update()
    {
        if(player.health <= 0)
        {
            SceneManager.LoadScene("GameOver Scene");
        }

        if (player.enemiesKilled >= 10) 
        {
            SceneManager.LoadScene("WinScene");
        }

    }

}
