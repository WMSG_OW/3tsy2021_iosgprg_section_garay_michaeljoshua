using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public GameObject[] weapons;

    public float xBounds, yBound;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnAweapon());
    }

    IEnumerator SpawnAweapon()
    {
        yield return new WaitForSeconds(3);
        Vector2 spawnPoint = new Vector2(Random.Range(-xBounds, xBounds), Random.Range(-yBound, yBound));
        if (GameObject.FindGameObjectsWithTag("Weapon").Length < 3) 
        Instantiate(weapons[Random.Range(0,weapons.Length)], spawnPoint , Quaternion.identity);
        StartCoroutine(SpawnAweapon());
    }
}
