using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenusManager : MonoBehaviour
{
   public  void onPlayButtonClicked()
    {
        SceneManager.LoadScene("GameScene");

    }

    public void onReturnToMenuClicked()
    {
        SceneManager.LoadScene("Main Menu");

    }

}
