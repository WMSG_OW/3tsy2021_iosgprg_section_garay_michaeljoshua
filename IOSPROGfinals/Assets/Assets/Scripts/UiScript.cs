using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UiScript : MonoBehaviour
{
    public TextMeshProUGUI weaponAmmotext;
    public TextMeshProUGUI healthText;
    public Player player;

    // Update is called once per frame
    void Update()
    {
        weaponAmmotext.text = "Ammo : " + player.currentWeapon.currentAmmo.ToString() + "/" + player.currentWeapon.ammoReserves.ToString();
        healthText.text = "Health : " + player.health.ToString();
    }
}
