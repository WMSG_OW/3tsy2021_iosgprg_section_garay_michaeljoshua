using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomLocation : MonoBehaviour
{
    public float changeRate;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("changeLocation", 2.0f, changeRate);
    }

    void changeLocation()
    {
        Vector2 newLocation = new Vector2(Random.Range(-10f, 10f), Random.Range(-15f, 15));
        this.transform.position = newLocation;
    }
}
