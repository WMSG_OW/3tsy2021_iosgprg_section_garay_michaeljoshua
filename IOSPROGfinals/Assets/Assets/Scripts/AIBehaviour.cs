using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class AIBehaviour : MonoBehaviour
{

    bool isShooting = false;
    public GameObject bulletPrefab;

    public int HealthAi = 100;

    public Player player;

    public AIDestinationSetter chasePlayer;
    private Patrol patrol;

    // Start is called before the first frame update
    void Start()
    {
        chasePlayer = GetComponent<AIDestinationSetter>();
        patrol = GetComponent<Patrol>();
    }

    private void Update()
    {
        if(HealthAi <= 0)
        {
            player.enemiesKilled++;
            Destroy(gameObject);
            Debug.Log(player.enemiesKilled);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isShooting = true;

        if(collision.gameObject.tag.Equals("Player"))
        {
            //chasePlayer.enabled = true;
            //patrol.enabled = false;
            if(isShooting == true)
            {
                InvokeRepeating("Shoot", 0.5f, 1.0f);
            }
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isShooting = false;

        if (collision.gameObject.tag.Equals("Player"))
        {
            //chasePlayer.enabled = false;
           // patrol.enabled = true;
        }
    }

    void Shoot()
    {
       GameObject bullet = Instantiate(bulletPrefab, GameObject.Find("FirepointAi").transform.position, Quaternion.identity);
    }


}
