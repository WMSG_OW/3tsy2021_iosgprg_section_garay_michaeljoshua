using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New weapon",menuName = "Weapon")]
public class Weapon : ScriptableObject
{
    public Sprite currentWeaponSprite;

    public GameObject bulletPrefab;

    public GameObject bulletSpawn;

    public int magSize;
    public int currentAmmo;
    public int ammoReserves;
    public float fireRate = 1;
    public int damage = 20;

    public string weaponType;

    public bool isMagEmpty;

    public void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, GameObject.Find("BulletSpawn").transform.position, Quaternion.identity);
    }

}
