using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickUp : MonoBehaviour
{
    public Weapon weapon;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {

            if(collision.GetComponent<Player>().backupWeapon == null)
            {
                collision.GetComponent<Player>().backupWeapon = weapon;
                Destroy(gameObject);
            }
            else
            {
                collision.GetComponent<Player>().currentWeapon = weapon;
                Destroy(gameObject);
            }
        }
    }
}
