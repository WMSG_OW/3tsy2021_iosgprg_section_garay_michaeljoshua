using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageAi : MonoBehaviour
{
   public AIBehaviour ai;

    public Player player;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Bullet"))
        {
            ai.HealthAi = ai.HealthAi - player.currentWeapon.damage;
            Debug.Log(ai.HealthAi);
        }

    }
}
