using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiBullet : MonoBehaviour
{
    private float speed = 5;

    private Vector2 Direction;

    void Start()
    {
      
        
            Direction = GameObject.Find("DirectionAi").transform.position;
            transform.position = GameObject.Find("FirepointAi").transform.position;
        
    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, Direction, speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D()
    {
        Destroy(gameObject);
    }


}
