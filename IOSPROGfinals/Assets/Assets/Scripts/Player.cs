using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Weapon currentWeapon;
    public Weapon backupWeapon;
    public Weapon temp;


    private Rigidbody2D rb;
    public float speed;
    private Vector2 moveVelocity;

    public Joystick moveJoystick;
    public Joystick shootJoyStick;

    public int health = 100;
    public  int enemiesKilled = 0;

    //just for AR
    bool isFiring;
    bool stopFiring;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        currentWeapon.currentAmmo = currentWeapon.magSize;
        Debug.Log(currentWeapon.currentAmmo);
        currentWeapon.isMagEmpty = false;
    }

    void Update()
    {
        Rotation();

        //bounds 
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -19.59f, 6.14f), Mathf.Clamp(transform.position.y, -14.82f, 4.6f));

        if(isFiring)
        {
            makeisFiringFalse();
            if (currentWeapon.isMagEmpty == false)
            {
                currentWeapon.Shoot();
                currentWeapon.currentAmmo--;
                Debug.Log(currentWeapon.currentAmmo);
            }
            else
            {
                Debug.Log("no ammo");
            }
        }

        if(currentWeapon.currentAmmo == 0)
        {
            currentWeapon.isMagEmpty = true;
            Debug.Log(currentWeapon.isMagEmpty);
        }

    }

    void FixedUpdate()
    {
        Movement();
    }

    void Rotation()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
        if(shootJoyStick.InputDirection != Vector3.zero)
        {
            angle = Mathf.Atan2(shootJoyStick.InputDirection.y, shootJoyStick.InputDirection.x) * Mathf.Rad2Deg - 90;
        }
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 10 * Time.deltaTime);
    }
    void Movement()
    {
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if(moveJoystick.InputDirection != Vector3.zero)
        {
            moveInput = moveJoystick.InputDirection;
        }

        moveVelocity = moveInput.normalized * speed;
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }

    public void onShootButtonClicked()
    {
        if (currentWeapon.weaponType == "Pistol" || currentWeapon.weaponType == "SG")
        {
            if (currentWeapon.isMagEmpty == false)
            {
                currentWeapon.Shoot();
                currentWeapon.currentAmmo--;
                Debug.Log(currentWeapon.currentAmmo);
            }
            else
            {
                Debug.Log("no ammo");
            }
        }
    }

    public void onReloadButtonClicked()
    {
        if (currentWeapon.ammoReserves > 0)
        {
            if (currentWeapon.ammoReserves < currentWeapon.magSize)
            {
                for (int i = -1; i <= currentWeapon.ammoReserves; i++)
                {
                    if(currentWeapon.currentAmmo != currentWeapon.magSize)
                    {
                        currentWeapon.currentAmmo++;
                        currentWeapon.ammoReserves--;
                    }
                }
                currentWeapon.isMagEmpty = false;
                Debug.Log(currentWeapon.currentAmmo);
                Debug.Log(currentWeapon.ammoReserves);
            }
            else
            {
                for (int i = 0; i < currentWeapon.magSize; i++)
                {
                    if (currentWeapon.currentAmmo != currentWeapon.magSize)
                    {
                        currentWeapon.currentAmmo++;
                        currentWeapon.ammoReserves--;
                    }
                }
                currentWeapon.isMagEmpty = false;
                Debug.Log(currentWeapon.currentAmmo);
                Debug.Log(currentWeapon.ammoReserves);
            }
        }
        else
        {
            Debug.Log("no ammo to reload");
        }
    }

    public void onSwapButtonClicked()
    {
        temp = currentWeapon;
        currentWeapon = backupWeapon;
        backupWeapon = temp;

    }

    public void onARshootDown()
    {
        if(currentWeapon.weaponType == "AR")
        {
            stopFiring = false;
            makeisFiringTrue();
        }
    }

    public void onARshootUp()
    {
        if (currentWeapon.weaponType == "AR")
        {
            isFiring = false;
            stopFiring = true;
        }
    }

    void makeisFiringTrue()
    {
        isFiring = true;
    }

    void makeisFiringFalse()
    {
        isFiring = false;
        if(stopFiring == false)
        { 
            Invoke("makeisFiringTrue", 0.1f);
        }
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("Bullet"))
        {
            health -= 10;
        }
    }

}
