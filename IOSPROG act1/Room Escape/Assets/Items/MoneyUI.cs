﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyUI : MonoBehaviour
{
    public Text Moneytxt;
    public static int Amount = 1000;

    void Start()
    {
        Moneytxt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Moneytxt.text = "MONEY: " + Amount.ToString();
    }
}
