﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Speed = 15f;

    private void Start()
    {
        Invoke("AutoDestroy", 1f);
    }

    void Update()
    {
        transform.position += transform.right * Time.deltaTime * Speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var enemy = collision.collider.GetComponent<EnemyAi>();

        if (enemy)
        {
            enemy.Takehit(1);
        }

        Destroy(gameObject);
    }

    void AutoDestroy()
    {
        Destroy(gameObject);
    }
}