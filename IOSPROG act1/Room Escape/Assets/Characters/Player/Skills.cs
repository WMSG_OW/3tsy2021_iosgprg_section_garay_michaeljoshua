﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skills
{
    public enum Skilltype
    {
        None,
        Shoot,
        Bribe,
        Dash,
        Sabotage,
    }

    private List<Skilltype> UnlockedSkillTypeList;
    public Skills()
    {
        UnlockedSkillTypeList = new List<Skilltype>();
    }
    private void UnlockSkill(Skilltype skilltype)
    {
        if (!isSkillUnlocked(skilltype))
        {
            UnlockedSkillTypeList.Add(skilltype);
        }
    }

    public bool isSkillUnlocked(Skilltype skilltype)
    {
        return UnlockedSkillTypeList.Contains(skilltype);
    }

    public Skilltype getSkillRequirement(Skilltype skilltype)
    {
        switch(skilltype)
        {
            case Skilltype.Bribe: return Skilltype.Shoot;
            case Skilltype.Dash: return Skilltype.Bribe;
            case Skilltype.Sabotage: return Skilltype.Dash;
        }
        return Skilltype.None;
    }

    public bool tryUnlockSkill(Skilltype skilltype)
    {
        Skilltype skillRequirement = getSkillRequirement(skilltype);

        if (skillRequirement != Skilltype.None)
        {
            if (isSkillUnlocked(skillRequirement))
            {
                UnlockSkill(skilltype);
                return true;
            }
            else return false;
        }
        else UnlockSkill(skilltype);
        return true;
    }
}
