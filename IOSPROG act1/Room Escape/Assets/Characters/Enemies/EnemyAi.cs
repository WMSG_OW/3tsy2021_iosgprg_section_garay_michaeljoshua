﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAi : MonoBehaviour
{


    public float maxhitPoints;
    public float hitPoints;

    public Transform spawnPos;
    public GameObject spawnee;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Seeker>().enabled = true;
        GetComponent<AIPath>().enabled = true;
        GetComponent<AIDestinationSetter>().enabled = false;
        GetComponent<Patrol>().enabled = true;

        hitPoints = maxhitPoints;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            GetComponent<Seeker>().enabled = true;
            GetComponent<AIPath>().enabled = true;
            GetComponent<AIDestinationSetter>().enabled = true;
            GetComponent<Patrol>().enabled = false;
            GetComponent<EnemyAi>().enabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            GetComponent<Seeker>().enabled = true;
            GetComponent<AIPath>().enabled = true;
            GetComponent<AIDestinationSetter>().enabled = false;
            GetComponent<Patrol>().enabled = true;
        }
    }

    public void Takehit(float damage)
    {
        hitPoints -= damage;
        if (hitPoints <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        Instantiate(spawnee, spawnPos.position, spawnPos.rotation);
    }
}
