﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private SkillButton Button;

    private void Start()
    {
        Button.SetPlayerSkills(player.GetSkills());
    }
}
