﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    //References
    Player player;

    public int InvinsibilityPoints = 5;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject.FindObjectOfType<Player>().InviPoints = InvinsibilityPoints;
            Destroy(gameObject);
            Debug.Log(GameObject.FindObjectOfType<Player>().InviPoints);

            if(GameObject.FindObjectOfType<Player>().InviPoints > InvinsibilityPoints)
            {
                GameObject.FindObjectOfType<Player>().InviPoints = 5;
            }

        }
    }

}
