﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class GameManager : MonoBehaviour
{
    public GameObject gameOverpanel;
    Enemy enemy;

    MenuManager playerSetup;


    public Transform playerSpawn;
    public Transform enemySpawn1;
    public Transform enemySpawn2;
    public Transform powerupSpawn;


    public GameObject enemyPrefab;
    public GameObject playerSpeedPrefab;
    public GameObject playerTankPreFab;
    public GameObject powerUpPrefab;

    public TextMeshProUGUI scoreText;
    public int score = 0;

    public GameObject newEnemy;
    public GameObject newEnemy2;
    public GameObject newPickUp;


    public bool isEnemy1Dead;
    public bool isEnemy2Dead;


    private void Start()
    {
        playerSetup = MenuManager.Instance;

        if(playerSetup.playerClass == "Speed")
        {
            Debug.Log("Spawning speed");
            GameObject newPlayer = Instantiate(playerSpeedPrefab, playerSpawn.transform.position, playerSpawn.transform.rotation);
            Camera.main.gameObject.GetComponent<CameraScript>().target = newPlayer.transform;
            //spawn speed player
        }
        else if(playerSetup.playerClass == "Tank")
        {
            Debug.Log("Spawning tank");
            GameObject newPlayer = Instantiate(playerTankPreFab, playerSpawn.transform.position, playerSpawn.transform.rotation);
            Camera.main.gameObject.GetComponent<CameraScript>().target = newPlayer.transform;
            //spawn tank player
        }

         newEnemy = Instantiate(enemyPrefab, enemySpawn1.transform.position, enemySpawn1.transform.rotation);
        newPickUp = Instantiate(powerUpPrefab, powerupSpawn.transform.position, powerupSpawn.transform.rotation);



        scoreText.text = "Score : " + score.ToString();
    }

    private void Update()
    {
        if (isEnemy1Dead == true && GameObject.FindObjectOfType<Player>().Tile2 == true)
        {
            newEnemy2 = Instantiate(enemyPrefab, enemySpawn2.transform.position, enemySpawn2.transform.rotation);
            isEnemy1Dead = false;
        }
        else if (isEnemy2Dead == true && GameObject.FindObjectOfType<Player>().Tile1 == true)
        {
            newEnemy = Instantiate(enemyPrefab, enemySpawn1.transform.position, enemySpawn1.transform.rotation);
            newPickUp = Instantiate(powerUpPrefab, powerupSpawn.transform.position, powerupSpawn.transform.rotation);
            isEnemy2Dead = false;
        }


        scoreText.text = "Score : " + score.ToString();
    }
    private void FixedUpdate()
    {
        if (GameObject.FindObjectOfType<Player>().isPlayerDead == true)
        {
            gameOverpanel.SetActive(true);
        }
    }

    public void onRetryButtonClicked()
    {
        SceneManager.LoadScene("GameScene");
    }

    

    
}
