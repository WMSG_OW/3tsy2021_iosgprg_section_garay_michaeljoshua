﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Player : MonoBehaviour
{
    public float moveSpeed = 6;

    private Vector2 startTouchposition;
    private Vector2 currentPosition;
    private Vector2 endTouchPosition;
    private bool stopTouch = false;

    public float swipeRange;
    public float tapRange;


    public float dashSpeed;
    public float startDashTime;
    private float dashTime;

    public Rigidbody2D rb;

    public int dNumber = 0;

    public bool isDashActive;
    public bool isPlayerDead;
    public bool Tile1;
    public bool Tile2;

    //powerups

    public int InviPoints;

    public float DDTimer = 5;
    public int DDMultiplier = 2;


    //enemy reference
    Enemy enemy;
    //public Enemy activeEnemy;

    public void Start()
    {
        dashTime = startDashTime;
        isDashActive = false;
        isPlayerDead = false;
        
    }


    public void Update()
    {
        Swipe();
        transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
       
    }


    public void Swipe()
    {

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchposition = Input.GetTouch(0).position;
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            currentPosition = Input.GetTouch(0).position;
            Vector2 Distance = currentPosition - startTouchposition;

            if (!stopTouch)
            {
                if (Distance.x < -swipeRange)
                {
                    Debug.Log("Left");
                    dNumber = 1;
                    GameObject.FindObjectOfType<Enemy>().takeDamage(dNumber);
                    stopTouch = true;
                }
                else if (Distance.x > swipeRange)
                {
                    Debug.Log("Right");
                    dNumber = 2;
                    GameObject.FindObjectOfType<Enemy>().takeDamage(dNumber);
                    stopTouch = true;
                }
                else if (Distance.y > swipeRange)
                {
                    Debug.Log("Up");
                    dNumber = 3;
                    GameObject.FindObjectOfType<Enemy>().takeDamage(dNumber);
                    stopTouch = true;
                }
                else if (Distance.y < -swipeRange)
                {
                    Debug.Log("Down");
                    dNumber = 4;
                    GameObject.FindObjectOfType<Enemy>().takeDamage(dNumber);
                    stopTouch = true;
                }
            }
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            stopTouch = false;

            endTouchPosition = Input.GetTouch(0).position;

            Vector2 Distance = endTouchPosition - startTouchposition;

            if (Mathf.Abs(Distance.x) < tapRange && Mathf.Abs(Distance.y) < tapRange)
            {
                Debug.Log("Tap");

                if (dashTime <= 0)
                {
                    dashTime = startDashTime;
                    rb.velocity = Vector2.zero;
                }
                else
                {
                    dashTime -= Time.time;

                    rb.velocity = Vector2.up * dashSpeed;
                    isDashActive = true;
                }
                isDashActive = false;

            }
        }

    }


    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Tile1"))
        {
            Tile1 = true;
            Tile2 = false;
            Debug.Log("T1 active");
        }
        else if (col.gameObject.CompareTag("Tile2"))
        {
            Tile1 = false;
            Tile2 = true;
            Debug.Log("T2 active");
        }
    }
    //Test Code for movement
    /* public void FixedUpdate()
     {
         if(Input.GetKey(KeyCode.UpArrow))
         {
             transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
         }

         if(Input.GetKey(KeyCode.DownArrow))
         {
             transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
         }
     }*/
}
