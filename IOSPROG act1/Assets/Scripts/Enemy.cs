﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    //randomize numbers 
    public int number;
    Player playerScript;
    public void RandomGenerate()
    {
        number = Random.Range(1, 4);
    }

    

    /*psuedo code
     
     if(number == dNumber from Player script)
    {

       Destroy(gameObject);

    }
     */

    public Transform arrowSpawn;
    public GameObject[] Arrows;
    // GameObject[] Enemies;

    public void takeDamage(int dNum)
    {
        if (GameObject.FindObjectOfType<Player>().Tile1 == true)
        {
            if (number == dNum)
            {
                GameObject.FindObjectOfType<GameManager>().score += 10;
                GameObject.FindObjectOfType<GameManager>().isEnemy1Dead = true;
                Destroy(GameObject.FindObjectOfType<GameManager>().newEnemy);
            }
        }
        else if(GameObject.FindObjectOfType<Player>().Tile2 == true)
        {
            if (number == dNum)
            {
                GameObject.FindObjectOfType<GameManager>().score += 10;
                GameObject.FindObjectOfType<GameManager>().isEnemy2Dead = true;
                Destroy(GameObject.FindObjectOfType<GameManager>().newEnemy2);
            }
        }
       
    }


    public void Start()
    {
        RandomGenerate();
        Debug.Log(number);
        Instantiate(Arrows[number - 1], arrowSpawn.transform.position, arrowSpawn.transform.rotation);
       // GameObject.FindObjectOfType<Player>().activeEnemy = this;
    }

    

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(GameObject.FindObjectOfType<Player>().isDashActive == true)
            {
                Destroy(gameObject);
                GameObject.FindObjectOfType<Player>().DDTimer = GameObject.FindObjectOfType<Player>().DDTimer - Time.deltaTime;
                GameObject.FindObjectOfType<GameManager>().score += 20;
            }
            else
            {
                GameObject.FindObjectOfType<Player>().isPlayerDead = true;
                Destroy(this);
            }
        }
    }

}
